import { Course } from "../classes/course";
import { Db, MongoClient } from "mongodb";
import 'dotenv/config';
import { Service } from "typedi";

@Service()
export class DatabaseService {
  private db: Db;
  private client: MongoClient;


  async start(url: string = process.env.DATABASE_URL!): Promise<void> {
    try {
      this.client = new MongoClient(url);
      await this.client.connect();
      this.db = this.client.db(process.env.DATABASE_NAME);
    } catch {
      throw new Error("Database connection error")
    }

    if (
      (await this.db.collection(process.env.DATABASE_COLLECTION!).countDocuments()) === 0
    ) {
      await this.populateDB();
    }
  }

  async closeConnection(): Promise<void> {
    return this.client.close();
  }

  async populateDB(): Promise<void> {
    const courses: Course[] = [
      {
        name: "Object Oriented Programming",
        credits: 3,
        subjectCode: "INF1010",
        teacher: "Samuel Kadoury",
      },
      {
        name: "Intro to Software Engineering",
        credits: 3,
        subjectCode: "LOG1000",
        teacher: "Bram Adams",
      },
      {
        name: "Project I",
        credits: 4,
        subjectCode: "INF1900",
        teacher: "Jerome Collin",
      },
      {
        name: "Project II",
        credits: 3,
        subjectCode: "LOG2990",
        teacher: "Levis Theriault",
      },
      {
        name: "Web Semantics and Ontology",
        credits: 2,
        subjectCode: "INF8410",
        teacher: "Michel Gagnon",
      },
    ];

    console.log("THIS ADDS DATA TO THE DATABASE, DO NOT USE OTHERWISE");
    for (const course of courses) {
      await this.db.collection(process.env.DATABASE_COLLECTION!).insertOne(course);
    }
  }

  get database(): Db {
    return this.db;
  }
}
