import "reflect-metadata";
import Container from "typedi";
import { Server } from "./server/server";

const server: Server = Container.get(Server);

server.init();
